# README #

This README is just to get you started.
Read the README.md files in the different folders instructions

### What is this repository for? ###

* This is a place to keep different tools to make [Ingress](http://ingress.com) Mission banners


### How do I get set up? ###

* Clone this repo, unzip, and read the README.md files in the different tools folder

### Contribution guidelines ###

* It's a free for all, but please fork, and push updates.

### Who do I talk to? ###

* Michael Hughson (Ingress IGN HuXdk)